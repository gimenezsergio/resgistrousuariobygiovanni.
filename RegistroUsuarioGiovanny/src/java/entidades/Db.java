package entidades;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Db {
     private static Db INSTANCE = null;
    //private static String LABASE = "jdbc:hsqldb:file:"+System.getProperty("user.home")+"/personas.hsqldb";
    private static String LABASE = "jdbc:mysql://10.0.0.191:3306/alumnos";
    private static String LABASEUSUARIO = "usuario";  // "root";
    private static String LABASECLAVE = "clave";    //"root";
    
    public static Db getInstance() throws ClassNotFoundException, IOException, SQLException {
        if (INSTANCE == null) {
            INSTANCE = new Db();
        }
        return INSTANCE;
    }
    private Db() throws ClassNotFoundException,
            IOException, SQLException {
    }

    public Connection getConnection() throws ClassNotFoundException,
            IOException, SQLException {
        Class.forName("org.mariadb.jdbc.Driver");
        return DriverManager.getConnection(LABASE, LABASEUSUARIO, LABASECLAVE);
    }
}
