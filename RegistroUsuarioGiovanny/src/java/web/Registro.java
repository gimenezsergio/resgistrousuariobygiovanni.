package web;

import com.google.gson.Gson;
import entidades.Db;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name="Registro", urlPatterns={"/Registro"})
public class Registro extends HttpServlet {
    Gson convertir = new Gson();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        try {            
            String consulta = "SELECT * FROM productos;";
            Connection con = Db.getInstance().getConnection();
            PreparedStatement pstm = con.prepareStatement(consulta);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getString(1) + " - " + rs.getString("precio"));
                
            }
            
        } catch (ClassNotFoundException exepcionDeGiovani) {
            System.out.println("Alerta: " + exepcionDeGiovani.getMessage());
        } catch (SQLException exepcionDeGiovani) {
            System.out.println("Alerta: " + exepcionDeGiovani.getMessage());
        }
        
        System.out.println("Capturamos el llamado del cliente por metodo GET"); 
        resp.getWriter().println("Felicitaciones, este es el registro de Giovanny.");
        
        
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("este es el POST");
        TreeMap resultado = convertir.fromJson(req.getReader().readLine(), TreeMap.class);
        String usuario = (String)resultado.get("usuario");
        String clave = (String)resultado.get("password");
        
        System.out.println(usuario + " - " +  clave);
    }
    
    
   
 

}
