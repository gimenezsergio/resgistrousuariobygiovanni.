package test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JDBCTest {

    public static void main(String[] args) throws SQLException {
        System.out.println("[...]");
        consultar();
        insertar();
        consultar();
        System.out.println("[OK]");
    }

    public static void consultar() {
        System.out.println("[...]");
        final String consulta = "SELECT * FROM productos";
        try {
            Connection myCon
                    = DriverManager.getConnection(
                            "jdbc:mysql://10.0.0.191:3306/alumnos",
                            "usuario",
                            "clave");
            PreparedStatement pstm = myCon.prepareStatement(consulta);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                System.out.println("Producto: " + rs.getString(1) + " Precio: " + rs.getString(2));

            }

        } catch (SQLException ex) {
            System.out.println("Alerta: " + ex.getMessage());
        }
        System.out.println("[OK]");
    }

    public static void insertar() {
        System.out.println("[...]");
        String consulta2 = "INSERT INTO productos(nombre,precio) VALUES('Motorola','11000')";
        try {
            Connection myCon
                    = DriverManager.getConnection(
                            "jdbc:mysql://10.0.0.191:3306/alumnos",
                            "usuario",
                            "clave");
            PreparedStatement pstm = myCon.prepareStatement(consulta2);
            pstm.executeQuery();

        } catch (SQLException ex) {
            System.out.println("Alerta: " + ex.getMessage());
        }

        System.out.println("[OK]");
    }
}
